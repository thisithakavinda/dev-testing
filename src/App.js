import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import LoginPage from './login/LoginPage';
function App() {
  return (
    <Router>
      <Routes>
        <Route path="/login" element={<LoginPage />} />
        {/* Add other routes here */}
        {/* <Route path="/" element={<Home />} /> Assuming you have a Home component */}
      </Routes>
    </Router>
  );
}

export default App;
