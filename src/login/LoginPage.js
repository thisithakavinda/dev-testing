import React, { useState } from 'react';
import './LoginPage.css';

function LoginPage() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [rememberMe, setRememberMe] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();
    // Handle login logic here
    console.log('Login attempted with:', { email, password, rememberMe });
  };

  return (
    <div className="login-container">
      <div className="login-form">
        <img src="/assets/soluzent-logo.png" alt="Soluzent Logo" className="logo" />
        <h2>Log in to continue</h2>
        <form onSubmit={handleSubmit}>
          <div className="form-group">
            <label htmlFor="email">Email</label>
            <input
              type="email"
              id="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              placeholder="Email"
              required
            />
          </div>
          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input
              type="password"
              id="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              placeholder="Password"
              required
            />
          </div>
          <div className="form-options">
            <label className="remember-me">
              <input
                type="checkbox"
                checked={rememberMe}
                onChange={(e) => setRememberMe(e.target.checked)}
              />
              Remember Me
            </label>
            <a href="/forgot-password" className="forgot-password">Forgot Password</a>
          </div>
          <button type="submit" className="login-button">Log in</button>
        </form>
        <p className="signup-prompt">Don't have an account? <a href="/signup">Sign up</a></p>
        <div className="social-login">
          <p>Or sign up with</p>
          <div className="social-icons">
            <button className="social-button facebook">
              <img src="/assets/facebook-icon.png" alt="Facebook" />
            </button>
            <button className="social-button instagram">
              <img src="/assets/instagram-icon.png" alt="Instagram" />
            </button>
            <button className="social-button google">
              <img src="/assets/google-icon.png" alt="Google" />
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LoginPage;